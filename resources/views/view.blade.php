<!DOCTYPE html>
<html lang="en">
<head>
    <title>Migração de Dados</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">


    <table class="table table-striped">
        <thead>
        <tr>
            <th>Id</th>
            <th>Modelo</th>
            <th>Código</th>
        </tr>
        </thead>
        <tbody>



            <tr>
                <td>{{$produto->id}}</td>
                <td>{{$produto->titulo}}</td>
                <td>{{$produto->codigo}}</td>
            </tr>

        </tbody>
    </table>
    <h2>Cores</h2>
    <table class="table table-striped">
        <thead>
            <th>Id</th>
            <th>Título</th>
            <th>Tamanhos</th>
        </thead>
        <tbody>
            @foreach($produto->cores as $cor)
                <tr>
                    <td>{{$cor->id}}</td>
                    <td>{{$cor->titulo}}</td>
                    <td>
                        @foreach(\App\Http\Helper\HelperCores::buscaTamanhos($cor,$produto->id) as $prod_tamanho)
                                @foreach($prod_tamanho as $prod)
                                    {{$prod->tamanho->titulo}} -
                                @endforeach
                         @endforeach
                    </td>
                </tr>
            @endforeach
        </tbody>

    </table>


</div>

</body>


</html>