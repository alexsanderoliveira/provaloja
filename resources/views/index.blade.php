<!DOCTYPE html>
<html lang="en">
<head>
    <title>Migração de Dados</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Id</th>
            <th>Modelo</th>
            <th>Código</th>
        </tr>
        </thead>
        <tbody>


        @foreach($produtos as $produto)
            <tr>
                <td>{{$produto->id}}</td>
                <td>{{$produto->titulo}}</td>
                <td>{{$produto->codigo}}</td>
                <td><a href="/produto/detalhes/{{$produto->id}}">Ver Detalhes do Produto</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <h2>Dados Antigos</h2>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Código</th>
            <th>Título</th>
            <th>Cor</th>
            <th>Tamanho</th>

        </tr>
        </thead>
        <tbody>


        @foreach($dadosAntigos as $produto)
            <tr>
                <td>{{$produto['codigo']}}</td>
                <td>{{$produto['titulo']}}</td>
                <td>{{$produto['cor']}}</td>
                <td>{{$produto['tamanho']}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <a href="/migrar" onclick="event.preventDefault();
    document.getElementById('migrar-form').submit();" type="button" class="btn btn-success">Migrar Dados</a>

    <form id="migrar-form" action="/migrar" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>

</div>

</body>


</html>