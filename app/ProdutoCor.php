<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdutoCor extends Model
{
    public $table = 'produtos_cores'; //Nome da tabela

    public $fillable = [
        'id',
        'id_produto',
        'id_cor',
    ];

    public $timestamps = false;

    public function Produto()
    {
        return $this->hasOne('App\Produto', 'id', 'id_produto');
    }

    public function Cor()
    {
        return $this->hasOne('App\Cor', 'id', 'id_cor');
    }

    public function Tamanhos()
    {
        return $this->belongsToMany('App\Cor', 'produtos_tamanhos', 'id_produto_cor', 'id' );
    }

}
