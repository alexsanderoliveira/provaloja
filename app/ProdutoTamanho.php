<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdutoTamanho extends Model
{
    public $table = 'produtos_tamanhos'; //Nome da tabela

    public $fillable = [
        'id',
        'id_produto_cor',
        'id_tamanho',
    ];
    public $timestamps = false;

    public function ProdutoCor()
    {
        return $this->hasOne('App\ProdutoCor', 'id', 'id_produto_cor');
    }

    public function Tamanho()
    {
        return $this->hasOne('App\Tamanho', 'id', 'id_tamanho');
    }
}
