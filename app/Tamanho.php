<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tamanho extends Model
{
    public $table = 'tamanhos';

    public $fillable = [
        'id',
        'titulo',
    ];
    public $timestamps = false;

}
