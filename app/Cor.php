<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cor extends Model
{
    public $table = 'cores';

    public $fillable = [
        'id',
        'titulo',
    ];
    public $timestamps = false;

}
