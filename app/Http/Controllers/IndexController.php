<?php

namespace App\Http\Controllers;

use App\Cor;
use App\DadosAntigos;
use App\Produto;
use Illuminate\Http\Request;

class IndexController extends Controller
{

    public function __construct(DadosAntigos $dadosAntigos,
                                Produto $produto)
    {
        $this->dadosAntigos = $dadosAntigos;
        $this->produto = $produto;
    }

    public function index(Request $request)
    {

        $dadosAntigos = $this->dadosAntigos->all()->toArray();
        $produtos = [];

        $input = $request->all();

        if($input){
            $produto = new Produto();
            $produto->cadastraArrayDeProdutos($dadosAntigos);
            $produtos = Produto::all();
        }

        return view('index', ['produtos' => $produtos, 'dadosAntigos' => $dadosAntigos]);
    }

    public function view($id)
    {
        $produto = Produto::find($id);

        if(!$produto){
             $produto = [];
        }

        return view('view', ['produto' => $produto]);
    }


}
