<?php
namespace App\Http\Helper;

use App\ProdutoCor;
use App\ProdutoTamanho;

class HelperCores {


    public static function buscaTamanhos($cor, $id_prod){

            $produto_cor = ProdutoCor::select()->where('id_produto', '=', $id_prod)->where('id_cor', '=', $cor->id)->first();

            if($produto_cor){
                $produtoTamanho [] = ProdutoTamanho::select()->where('id_produto_cor', '=', $produto_cor->id)->get();
            }

        return $produtoTamanho;
    }
}