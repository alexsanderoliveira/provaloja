<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    public $table = 'produtos';

    public $fillable = [
        'id',
        'titulo',
        'codigo',
    ];
    public $timestamps = false;

    public function cores()
    {
        return $this->belongsToMany('App\Cor', 'produtos_cores', 'id_produto', 'id_cor' );
    }


    public function buscaProduto($cod){
        $produto = self::select()
            ->where('codigo', '=', $cod)->first();
        return $produto;
    }

    public function cadastraArrayDeProdutos(array $produtosArray){

        foreach ($produtosArray as $prod) {
            $produto = $this->buscaProduto($prod['codigo']);
            if($produto){
                $this->verificaProdutoCadastra($produto, $prod);
            } else{
                $this->cadastraProduto($prod);
            }
        }
    }


    public function verificaProdutoCadastra(Produto $prod, $produto){
        try {
            //abri uma transação
            \DB::beginTransaction();
        //verifica se existe a cor cadastrada
        $cor = Cor::firstOrCreate(array('titulo' => $produto['cor']));

        if(!is_object($cor)){
            //salva a cor

            $id_cor = \DB::getPdo()->lastInsertId();
            if(ProdutoCor::create(['id_produto' => $prod->id, 'id_cor' => $id_cor])) {
                $id_produto_cor = \DB::getPdo()->lastInsertId();
            }
            $tamanho = Tamanho::firstOrCreate(array('titulo' =>$produto['tamanho']));

            if($tamanho){
                $id_tamanho = is_object($tamanho) ? $tamanho->id : \DB::getPdo()->lastInsertId();

                $produto = ProdutoTamanho::firstOrCreate(array('id_produto_cor' => $id_produto_cor, 'id_tamanho' => $id_tamanho));

            }

        }
        else{
            $produto_cor = ProdutoCor::firstOrCreate(['id_produto' => $prod->id, 'id_cor' => $cor->id]);
            if($produto_cor) {
                $id_produto_cor = is_object($produto_cor) ? $produto_cor->id : \DB::getPdo()->lastInsertId();
            }
            $tamanho = Tamanho::firstOrCreate(array('titulo' => $produto['tamanho']));
            if($tamanho){
                $id = (is_object($tamanho)) ? $tamanho->id : \DB::getPdo()->lastInsertId();
                $produto_tamanho = ProdutoTamanho::firstOrCreate(array('id_produto_cor' => $id_produto_cor, 'id_tamanho' => $id));
            }
        }
            \DB::commit();
        } catch (\Exception $e) {
            // como deu erro em alguma etapa faço o rollback
            \DB::rollback();
            print $e->getMessage();
            return false;
        }

        return true;

    }



    public function cadastraProduto($produto) {
        try {
            //abri uma transação
            \DB::beginTransaction();

            $prod = array();
            $prod['codigo'] = $produto['codigo'];
            $prod['titulo'] = $produto['titulo'];
            //salva o produto

            if(self::create($prod)) {
                $id_produto = \DB::getPdo()->lastInsertId();
            }

            //salva a cor

            $cor = Cor::firstOrCreate(array('titulo' => $produto['cor']));

            if($cor){
                $id_cor = is_object($cor) ? $cor->id : \DB::getPdo()->lastInsertId();
            }

            if(ProdutoCor::create(['id_produto' => $id_produto, 'id_cor' => $id_cor])) {
                $id_produto_cor = \DB::getPdo()->lastInsertId();
            }
            $tamanho = Tamanho::firstOrCreate(array('titulo' => $produto['tamanho']));
            if($tamanho) {
                $id_tamanho = is_object($tamanho) ? $tamanho->id : \DB::getPdo()->lastInsertId();
            }

            ProdutoTamanho::create(['id_produto_cor' => $id_produto_cor, 'id_tamanho' => $id_tamanho]);

            //finalizei com sucesso a transação
            \DB::commit();
        } catch (\Exception $e) {
            // como deu erro em alguma etapa faço o rollback
            \DB::rollback();
            print $e->getMessage();
            return false;
        }

        return true;

    }

}
